// DBGHelperView.h : interface of the CDBGHelperView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DBGHELPERVIEW_H__6445EAD1_B3BE_4F9E_9C4F_66A85DFC861B__INCLUDED_)
#define AFX_DBGHELPERVIEW_H__6445EAD1_B3BE_4F9E_9C4F_66A85DFC861B__INCLUDED_

#include "Serial.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CDBGHelperView : public CEditView
{
protected: // create from serialization only
	CDBGHelperView();
	DECLARE_DYNCREATE(CDBGHelperView)

// Attributes
public:
	CDBGHelperDoc* GetDocument();
	CSerial m_hDevice;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDBGHelperView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL Open();
	CFont*  m_pFont;
	static DWORD WINAPI ReceiveThread(LPVOID pParam);
	virtual ~CDBGHelperView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CDBGHelperView)
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnEditPaste();
	afx_msg void OnClearView();
	afx_msg void OnOk();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnViewSetFont();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in DBGHelperView.cpp
inline CDBGHelperDoc* CDBGHelperView::GetDocument()
   { return (CDBGHelperDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBGHELPERVIEW_H__6445EAD1_B3BE_4F9E_9C4F_66A85DFC861B__INCLUDED_)
