// DBGHelperView.cpp : implementation of the CDBGHelperView class
//

#include "stdafx.h"
#include "DBGHelper.h"
#include "Serial.h"

#include "DBGHelperDoc.h"
#include "DBGHelperView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperView

IMPLEMENT_DYNCREATE(CDBGHelperView, CEditView)

BEGIN_MESSAGE_MAP(CDBGHelperView, CEditView)
	//{{AFX_MSG_MAP(CDBGHelperView)
	ON_WM_CHAR()
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_BN_CLICKED(IDC_CLEAR_VIEW, OnClearView)
	ON_BN_CLICKED(ID_OK, OnOk)
	ON_WM_RBUTTONUP()
	ON_COMMAND(ID_VIEW_SET_FONT, OnViewSetFont)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CEditView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperView construction/destruction

CDBGHelperView::CDBGHelperView()
{
	CDialogBar* pDlgbar;
	char		szDeviceName[16];

	((CMainFrame *)AfxGetMainWnd())->SetDcb();
	pDlgbar = &((CMainFrame *)AfxGetMainWnd())->m_wndDialogBarBottom;

	pDlgbar->GetDlgItem(IDC_PORT_LIST)->GetWindowText(szDeviceName, 16);

//	((CMainFrame *)AfxGetMainWnd())->OnOpenPort();
	if(Open())
	{
		m_hDevice.m_nReceivedBytes = 0;
	}
	else
	{
		CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
		CBitmap		bmpLoader;

		pFrame->m_wndDialogBarBottom.GetDlgItem(IDC_OPEN_PORT)->SetWindowText("打开串口(&O)");
		bmpLoader.LoadBitmap(IDB_PORT_CLOSE);
		((CStatic *)pFrame->m_wndDialogBarBottom.GetDlgItem(IDC_PORT_STAT))->SetBitmap((HBITMAP)bmpLoader.Detach());
		MessageBox("Comm Port Open failed!");
	}
	if(!CreateThread(NULL, NULL, ReceiveThread, this, 0, NULL))
	{
		MessageBox("Failed to create thread: ReceiveThread");
	}

}

CDBGHelperView::~CDBGHelperView()
{
	m_hDevice.Close();
	delete m_pFont;
}

BOOL CDBGHelperView::PreCreateWindow(CREATESTRUCT& cs)
{
	BOOL bPreCreated = CEditView::PreCreateWindow(cs);
	cs.style &= ~(ES_AUTOHSCROLL|WS_HSCROLL);	// Enable word-wrapping

	return bPreCreated;
}

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperView drawing

void CDBGHelperView::OnDraw(CDC* pDC)
{
	CDBGHelperDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
}

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperView printing

BOOL CDBGHelperView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default CEditView preparation
	return CEditView::OnPreparePrinting(pInfo);
}

void CDBGHelperView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CEditView::OnBeginPrinting(pDC, pInfo);
}

void CDBGHelperView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CEditView::OnEndPrinting(pDC, pInfo);
}

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperView diagnostics

#ifdef _DEBUG
void CDBGHelperView::AssertValid() const
{
	CEditView::AssertValid();
}

void CDBGHelperView::Dump(CDumpContext& dc) const
{
	CEditView::Dump(dc);
}

CDBGHelperDoc* CDBGHelperView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDBGHelperDoc)));
	return (CDBGHelperDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperView message handlers

void CDBGHelperView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CMainFrame * pFrame = (CMainFrame *)AfxGetMainWnd();
	
	if(!m_hDevice.IsOpened())
	{
		pFrame->OnOpenPort();
	}
	// TODO: Add your message handler code here and/or call default
	if(m_hDevice.IsOpened())
	{
		m_hDevice.WriteCommByte(nChar);
	}

	GetEditCtrl().SetSel(GetEditCtrl().GetWindowTextLength(), -1, FALSE);

//	CEditView::OnChar(nChar, nRepCnt, nFlags);
}

void ErrorTrace(DWORD	ErrorCode)
{
	LPVOID	lpMsgBuf;
	LPVOID	lpDisplayBuf;

	FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        ErrorCode,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );
	// Display the error message and exit the process
	
    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, 
        (lstrlen((LPCTSTR)lpMsgBuf)+40)*sizeof(TCHAR)); 
	sprintf((LPSTR)lpDisplayBuf,
		"failed with error %d: %s",
		ErrorCode,
		(LPCTSTR)lpMsgBuf);
	TRACE((LPCTSTR)lpDisplayBuf);
    //MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK); 
	
    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);

}

void BinToHex( char *sIn, char *sOut, int len )
{
	int		i;
	byte	high, low;
	
	for ( i=0; i<len; i++ )
	{
		low		= (( byte )*sIn ) & 0x0f;
		high	= (( byte )*sIn ) >> 4;
		
		*( byte * )sOut = high<10 ? high+'0' : high+'7';
		sOut++;
		
		*( byte * )sOut = low<10 ? low+'0' : low+'7';
		sOut++;

		*( byte * )sOut = ' ';
		sOut++;
		
		sIn++;
	}
}

DWORD WINAPI CDBGHelperView::ReceiveThread(LPVOID pParam)
{
	BOOL	bResult = TRUE;
	char	szTemp[16];
	char*	psBuf;
	DWORD	dwEventMask = 0;
	DWORD	dwError;
	COMSTAT	ComStat;
	OVERLAPPED	Overlapped;
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CDBGHelperView* pView = (CDBGHelperView *)pParam;

	Overlapped.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	ResetEvent(Overlapped.hEvent);
	Overlapped.Internal = 0;
    Overlapped.InternalHigh = 0;
    Overlapped.Offset = 0;
    Overlapped.OffsetHigh = 0;
	
    assert(Overlapped.hEvent);

	for (;;) 
	{ 

		// Make a call to WaitCommEvent().  This call will return immediatly
		// because our port was created as an async port (FILE_FLAG_OVERLAPPED
		// and an m_OverlappedStructerlapped structure specified).  This call will cause the 
		// m_OverlappedStructerlapped element m_OverlappedStruct.hEvent, which is part of the m_hEventArray to 
		// be placed in a non-signeled state if there are no bytes available to be read,
		// or to a signeled state if there are bytes available.  If this event handle 
		// is set to the non-signeled state, it will be set to signeled when a 
		// character arrives at the port.

		// we do this for each port!
		if (!pView->m_hDevice.IsOpened())
		{
			Sleep(100);
			continue;
		}

		bResult = WaitCommEvent(pView->m_hDevice.m_hIDComDev, &dwEventMask, &Overlapped);

		if (!bResult)  
		{ 
			// If WaitCommEvent() returns FALSE, process the last error to determin
			// the reason..
			switch (dwError = GetLastError()) 
			{ 
			case ERROR_IO_PENDING: 	
				{ 
					// This is a normal return value if there are no bytes
					// to read at the port.
					// Do nothing and continue
					ErrorTrace(dwError);
					break;
				}
			case 87:
				{
					// Under Windows NT, this value is returned for some reason.
					// I have not investigated why, but it is also a valid reply
					// Also do nothing and continue.
					ErrorTrace(dwError);
					break;
				}
			default:
				{
					// All other error codes indicate a serious error has
					// occured.  Process this error.
//					MessageBox("WaitCommEvent()");
					ErrorTrace(dwError);
					ClearCommError(pView->m_hDevice.m_hIDComDev, &dwError, &ComStat);
					continue;
					break;
				}
			}
//			continue;
		}
		else
		{
			// If WaitCommEvent() returns TRUE, check to be sure there are
			// actually bytes in the buffer to read.  
			//
			// If you are reading more than one byte at a time from the buffer 
			// (which this program does not do) you will have the situation occur 
			// where the first byte to arrive will cause the WaitForMultipleObjects() 
			// function to stop waiting.  The WaitForMultipleObjects() function 
			// resets the event handle in m_OverlappedStruct.hEvent to the non-signelead state
			// as it returns.  
			//
			// If in the time between the reset of this event and the call to 
			// ReadFile() more bytes arrive, the m_OverlappedStruct.hEvent handle will be set again
			// to the signeled state. When the call to ReadFile() occurs, it will 
			// read all of the bytes from the buffer, and the program will
			// loop back around to WaitCommEvent().
			// 
			// At this point you will be in the situation where m_OverlappedStruct.hEvent is set,
			// but there are no bytes available to read.  If you proceed and call
			// ReadFile(), it will return immediatly due to the async port setup, but
			// GetOverlappedResults() will not return until the next character arrives.
			//
			// It is not desirable for the GetOverlappedResults() function to be in 
			// this state.  The thread shutdown event (event 0) and the WriteFile()
			// event (Event2) will not work if the thread is blocked by GetOverlappedResults().
			//
			// The solution to this is to check the buffer with a call to ClearCommError().
			// This call will reset the event handle, and if there are no bytes to read
			// we can loop back through WaitCommEvent() again, then proceed.
			// If there are really bytes to read, do nothing and proceed.
		
			bResult = ClearCommError(pView->m_hDevice.m_hIDComDev, &dwError, &ComStat);

			if (ComStat.cbInQue == 0)
				continue;
		}	// end if bResult

		// 主等待函数,等待事件发生,他正常地阻塞线程,直到要求的事件发生
		dwError = WaitForSingleObject(Overlapped.hEvent, INFINITE);
		switch(dwError)
		{
		case WAIT_ABANDONED:
			TRACE("WAIT_ABANDONED\n");
			break;
		case WAIT_OBJECT_0:
			TRACE("WAIT_OBJECT_0\n");
			break;
		case WAIT_TIMEOUT:
			TRACE("WAIT_TIMEOUT\n");
			break;
		default:
			TRACE("WaitForSingleObject Error\n");
			dwError = GetLastError();
			ErrorTrace(dwError);
			break;
		}
		if(!GetCommMask(pView->m_hDevice.m_hIDComDev, &dwEventMask)) 
			continue;

		switch (dwEventMask)
		{
		case	EV_RXCHAR:
			bResult = ClearCommError(pView->m_hDevice.m_hIDComDev, &dwError, &ComStat);
			
			if (ComStat.cbInQue == 0)//输入缓冲区无字符
				continue;
			break;
		default:
			continue;
		}

		// Main wait function.  This function will normally block the thread
		// until one of nine events occur that require action.
//		TRACE("Get data size = %d\n", ComStat.cbInQue);
		psBuf = new char[ComStat.cbInQue + 1];
		pView->m_hDevice.m_nReceivedBytes += pView->m_hDevice.ReadData(psBuf, ComStat.cbInQue);
		psBuf[ComStat.cbInQue] = 0;
		SCROLLINFO ScrollInfo;
		pView->GetScrollInfo(SB_VERT, &ScrollInfo);

//		CScrollBar *tmp = pView->GetScrollBarCtrl(SB_VERT);
//		if(pView->GetScrollBarCtrl(SB_VERT)->GetScrollPos() < pView->GetScrollBarCtrl(SB_VERT)->GetScrollLimit())
		if(ScrollInfo.nPos < ScrollInfo.nMax+1 - (int)ScrollInfo.nPage - 5)
		{
			pView->SetRedraw(FALSE);
		}
		if (pFrame->m_wndDialogBarBottom.IsDlgButtonChecked(IDC_VIEW_HEX))
		{
			char	*psHexbuf = new char[ComStat.cbInQue*3 + 1];
			
			BinToHex(psBuf, psHexbuf, ComStat.cbInQue);
			psHexbuf[ComStat.cbInQue*3] = 0;

			pView->GetEditCtrl().SetSel(pView->GetWindowTextLength(),-1);
			pView->GetEditCtrl().ReplaceSel(psHexbuf);
		}
		else
		{
			if(((ComStat.cbInQue == 1) && (psBuf[0] == 0x08))
				|| ((ComStat.cbInQue == 3) && (psBuf[0] == 0x08) && (psBuf[1] == 0x20) && (psBuf[2] == 0x08)))
			{
				pView->GetEditCtrl().SetSel(pView->GetWindowTextLength()-1, pView->GetWindowTextLength());
				pView->GetEditCtrl().ReplaceSel("");
				
			}
			else if((ComStat.cbInQue == 1) && (psBuf[0] == 0))
			{
			}
			else
			{
				pView->GetEditCtrl().SetSel(pView->GetWindowTextLength(),-1);
				pView->GetEditCtrl().ReplaceSel(psBuf);
			}
		}
		if(ScrollInfo.nPos < ScrollInfo.nMax+1 - (int)ScrollInfo.nPage - 5)
		{
			//CScrollBar *SBVert = pView->GetEditCtrl().GetScrollBarCtrl(SB_VERT);
			//SBVert->SetRedraw(TRUE);
			pView->SetRedraw(TRUE);
		}

		delete psBuf;

		sprintf(szTemp, "Rx: %08d byte", pView->m_hDevice.m_nReceivedBytes);
		((CMainFrame *)AfxGetMainWnd())->m_wndStatusBar.GetStatusBarCtrl().SetText(szTemp, 2, 0);
#if 0
		Event = WaitForMultipleObjects(3, port->m_hEventArray, FALSE, INFINITE);

		switch (Event)
		{
		case 0:
			{
				// Shutdown event.  This is event zero so it will be
				// the higest priority and be serviced first.

			 	port->m_bThreadAlive = FALSE;
				
				// Kill this thread.  break is not needed, but makes me feel better.
				AfxEndThread(100);
				break;
			}
		case 1:	// read event
			{
				GetCommMask(port->m_hComm, &CommEvent);
				if (CommEvent & EV_CTS)
					::SendMessage(port->m_pOwner->m_hWnd, WM_COMM_CTS_DETECTED, (WPARAM) 0, (LPARAM) port->m_nPortNr);
				if (CommEvent & EV_RXFLAG)
					::SendMessage(port->m_pOwner->m_hWnd, WM_COMM_RXFLAG_DETECTED, (WPARAM) 0, (LPARAM) port->m_nPortNr);
				if (CommEvent & EV_BREAK)
					::SendMessage(port->m_pOwner->m_hWnd, WM_COMM_BREAK_DETECTED, (WPARAM) 0, (LPARAM) port->m_nPortNr);
				if (CommEvent & EV_ERR)
					::SendMessage(port->m_pOwner->m_hWnd, WM_COMM_ERR_DETECTED, (WPARAM) 0, (LPARAM) port->m_nPortNr);
				if (CommEvent & EV_RING)
					::SendMessage(port->m_pOwner->m_hWnd, WM_COMM_RING_DETECTED, (WPARAM) 0, (LPARAM) port->m_nPortNr);
				
				if (CommEvent & EV_RXCHAR)
					// Receive character event from port.
					ReceiveChar(port, comstat);
					
				break;
			}  
		case 2: // write event
			{
				// Write character event from port
				WriteChar(port);
				break;
			}
			
		} // end switch
#endif
	} // close forever loop
	CloseHandle(Overlapped.hEvent);
	return 0;
}

void CDBGHelperView::OnEditPaste() 
{
	HANDLE	hClip;
	char	*psBuf;

	if(!m_hDevice.IsOpened() && !Open())
	{
		return;
	}
	if(!OpenClipboard())
	{
		return;
	}
	hClip = ::GetClipboardData(CF_TEXT);
	psBuf=(char *)GlobalLock(hClip);   //锁定数据
	GlobalUnlock(hClip);               //解锁
	CloseClipboard();
	if(psBuf == NULL)return;
	if(GetFocus() != this)
	{
		::SendMessage(GetFocus()->m_hWnd, WM_PASTE, NULL, NULL);
	}
	else
	{
		m_hDevice.WriteComm(psBuf, strlen(psBuf));
	}
	
}

BOOL CDBGHelperView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	
    if(pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case	VK_ESCAPE:
		case	VK_BACK:
			m_hDevice.WriteCommByte(pMsg->wParam);
			goto done;
		case	VK_UP:
			m_hDevice.WriteComm("\x1b\x5b", 2);
			m_hDevice.WriteCommByte(0x41);
			goto done;
		case	VK_DOWN:
			m_hDevice.WriteComm("\x1b\x5b", 2);
			m_hDevice.WriteCommByte(0x42);
			goto done;
		case	VK_LEFT:
			m_hDevice.WriteComm("\x1b\x5b", 2);
			m_hDevice.WriteCommByte(0x44);
			goto done;
		case	VK_RIGHT:
			m_hDevice.WriteCommByte(pMsg->wParam);
			m_hDevice.WriteCommByte(0x43);
			goto done;
		case	'C':
			if (GetAsyncKeyState(VK_CONTROL) & 0x8000)
			{
				m_hDevice.WriteCommByte(0x03);
				goto done;
			}
		default:
			break;
		}
	}

	return CEditView::PreTranslateMessage(pMsg);
done:
	return TRUE;
}

void CDBGHelperView::OnClearView() 
{
	SetWindowText("");
	((CMainFrame *)AfxGetMainWnd())->m_wndStatusBar.GetStatusBarCtrl().SetText( "Rx: 00000000 byte", 2, 0 );
	((CMainFrame *)AfxGetMainWnd())->m_wndStatusBar.GetStatusBarCtrl().SetText( "Tx: 00000000 byte", 3, 0 );
}


void CDBGHelperView::OnInitialUpdate() 
{
	GetParentFrame()->ShowWindow(SW_SHOWMAXIMIZED);  
	m_pFont = new CFont;
// 	m_pFont->CreateFont(9, // nHeight 
// 		8, // nWidth 
// 		0, // nEscapement 
// 		0, // nOrientation 
// 		FW_THIN, // nWeight 
// 		FALSE, // bItalic 
// 		FALSE, // bUnderline 
// 		0, // cStrikeOut 
// 		ANSI_CHARSET, // nCharSet 
// 		OUT_DEFAULT_PRECIS, // nOutPrecision 
// 		CLIP_DEFAULT_PRECIS, // nClipPrecision 
// 		DEFAULT_QUALITY, // nQuality 
// 		DEFAULT_PITCH | FF_SWISS, // nPitchAndFamily 
// 		_T("MS Sans Serif"));
	m_pFont->CreatePointFont(100, _T("宋体"));
	SetFont(m_pFont);
	GetEditCtrl().SetLimitText(-1);
	CEditView::OnInitialUpdate();
}

void CDBGHelperView::OnOk() 
{
	int	nTxtlen;
	nTxtlen = GetEditCtrl().GetWindowTextLength();
	
	GetEditCtrl().SetSel(nTxtlen - 1, -1);
}

void CDBGHelperView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	//设置为焦点
	SetFocus();
	
	//创建一个弹出式菜单
	CMenu popmenu;
	popmenu.CreatePopupMenu();
	
	//添加菜单项目
	
	//	popmenu.AppendMenu(0, ID_EDIT_UNDO, "&Undo");
	//	popmenu.AppendMenu(0, MF_SEPARATOR);
	//	popmenu.AppendMenu(0, ID_EDIT_CUT, "剪切(&T)");
	popmenu.AppendMenu(0, ID_EDIT_COPY, "复制(&C)");
	popmenu.AppendMenu(0, ID_EDIT_PASTE, "粘贴(&P)");
	popmenu.AppendMenu(0, MF_SEPARATOR);
	popmenu.AppendMenu(0, ID_EDIT_CLEAR, "清除(&L)");
	popmenu.AppendMenu(0, ID_EDIT_SELECT_ALL, "全选(&A)");
	//	popmenu.AppendMenu(0, MF_SEPARATOR);
	
	//初始化菜单项
	//	UINT nUndo=(CanUndo() ? 0 : MF_GRAYED );
	//	popmenu.EnableMenuItem(ID_EDIT_UNDO, MF_BYCOMMAND|nUndo);
	CString s;
	GetSelectedText(s);
	UINT nSel=((s != "") ? 0 : MF_GRAYED) ;
	//	popmenu.EnableMenuItem(ID_EDIT_CUT, MF_BYCOMMAND|nSel);
	popmenu.EnableMenuItem(ID_EDIT_COPY, MF_BYCOMMAND|nSel);
	popmenu.EnableMenuItem(ID_EDIT_CLEAR, MF_BYCOMMAND|nSel);
	UINT nPaste=0;//(CanPaste() ? 0 : MF_GRAYED) ;
	popmenu.EnableMenuItem(ID_EDIT_PASTE, MF_BYCOMMAND|nPaste);
	
	//显示菜单
	CPoint pt;
	GetCursorPos(&pt);
	popmenu.TrackPopupMenu(TPM_RIGHTBUTTON, pt.x, pt.y, this);
	popmenu.DestroyMenu();
	
//	CEditView::OnRButtonUp(nFlags, point);
}

void CDBGHelperView::OnViewSetFont() 
{
	CFontDialog	dlg;
	
	m_pFont->GetLogFont(dlg.m_cf.lpLogFont);
	dlg.m_cf.Flags |= CF_INITTOLOGFONTSTRUCT;

	//	m_pFont->GetLogFont(dlg.m_cf.lpLogFont);
	if(dlg.DoModal() == IDOK)
	{
		m_pFont->DeleteObject();
		m_pFont->CreateFontIndirect(dlg.m_cf.lpLogFont);
		SetTextColor(GetDC()->m_hDC, dlg.GetColor());
		SetFont(m_pFont);
	}
}

BOOL CDBGHelperView::Open()
{
	char		szDeviceName[16];
	char		szBaud[16];
	int			nBaud;
	CDialogBar*	pDlgbar;
	
	pDlgbar = &((CMainFrame *)AfxGetMainWnd())->m_wndDialogBarBottom;
	pDlgbar->GetDlgItem(IDC_PORT_LIST)->GetWindowText(szDeviceName, 15);
	pDlgbar->GetDlgItem(IDC_SET_BITRATE)->GetWindowText(szBaud, 15);
	nBaud = atoi(szBaud);
		
	return m_hDevice.Open(szDeviceName, nBaud);
}
