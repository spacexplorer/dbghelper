// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "DBGHelper.h"
#include "dbt.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "DBGHelperDoc.h"
#include "DBGHelperView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_OPEN_FILE, OnOpenFile)
	ON_BN_CLICKED(IDC_VIEW_HELP, OnViewHelp)
	ON_CBN_SELCHANGE(IDC_PORT_LIST, OnSelchangePortList)
	ON_BN_CLICKED(IDC_EXPAND_WIN, OnExpandWin)
	ON_BN_CLICKED(IDC_INPUT_SEND, OnInputSend)
	ON_BN_CLICKED(IDC_SEND_FILE, OnSendFile)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_BUTTON6, OnButton6)
	ON_BN_CLICKED(IDC_BUTTON7, OnButton7)
	ON_BN_CLICKED(IDC_BUTTON8, OnButton8)
	ON_BN_CLICKED(IDC_BUTTON9, OnButton9)
	ON_BN_CLICKED(IDC_BUTTON10, OnButton10)
	ON_BN_CLICKED(IDC_BUTTON11, OnButton11)
	ON_BN_CLICKED(IDC_BUTTON12, OnButton12)
	ON_BN_CLICKED(IDC_BUTTON13, OnButton13)
	ON_BN_CLICKED(IDC_BUTTON14, OnButton14)
	ON_BN_CLICKED(IDC_BUTTON15, OnButton15)
	ON_BN_CLICKED(IDC_BUTTON16, OnButton16)
	ON_BN_CLICKED(IDC_BUTTON17, OnButton17)
	ON_BN_CLICKED(IDC_BUTTON32, OnButton32)
	ON_BN_CLICKED(IDC_BUTTON20, OnButton20)
	ON_BN_CLICKED(IDC_BUTTON21, OnButton21)
	ON_BN_CLICKED(IDC_BUTTON22, OnButton22)
	ON_BN_CLICKED(IDC_BUTTON23, OnButton23)
	ON_BN_CLICKED(IDC_BUTTON24, OnButton24)
	ON_BN_CLICKED(IDC_BUTTON25, OnButton25)
	ON_BN_CLICKED(IDC_BUTTON26, OnButton26)
	ON_BN_CLICKED(IDC_BUTTON27, OnButton27)
	ON_BN_CLICKED(IDC_BUTTON28, OnButton28)
	ON_BN_CLICKED(IDC_BUTTON29, OnButton29)
	ON_BN_CLICKED(IDC_BUTTON30, OnButton30)
	ON_BN_CLICKED(IDC_BUTTON31, OnButton31)
	ON_BN_CLICKED(IDC_BUTTON18, OnButton18)
	ON_BN_CLICKED(IDC_BUTTON19, OnButton19)
	ON_BN_CLICKED(IDC_INPUT_SETFOCUS, OnInputSetfocus)
	ON_WM_LBUTTONUP()
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_BN_CLICKED(IDC_ITEM_CLEAR, OnItemClear)
	ON_CBN_SELCHANGE(IDC_SET_BITRATE, OnSelchangeSetBitrate)
	ON_BN_CLICKED(IDC_ENABLE_TIMER, OnEnableTimer)
	ON_BN_CLICKED(IDC_OPEN_PORT, OnOpenPort)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_COPYRIGHT,
	ID_RX_COUNT,
	ID_TX_COUNT,
	ID_DEVICE_CONFIG,
	ID_DEVICE_STATUS,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

CtlTable_t stCtlTable[] = {
	{"PortName", IDC_PORT_LIST},
	{"BaudRate", IDC_SET_BITRATE},
	{"Databits", IDC_SET_DATABIT},
	{"Stopbits", IDC_SET_STOPBIT},
	{"Verifybit", IDC_SET_VERIFYBIT},
	{"Flowctl", IDC_SET_FLOWCTL},
};


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
}

CMainFrame::~CMainFrame()
{
}
#define	MAX_COMM_NUM	10
#define	MAX_COMM_NAME	16
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	int		nPortNum = 0;
	char		aListStr[MAX_COMM_NUM][MAX_COMM_NAME];
	CComboBox*	pPortList;

	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
#if 0
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
#endif

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	if ( !m_wndDialogBarBottom.Create( this, IDD_DIALOGBAR_BOTTOM, CBRS_BOTTOM | WS_VISIBLE, IDD_DIALOGBAR_BOTTOM ))
	{
		TRACE0("Failed to create dialogbar\n");
		return -1;      // fail to create
	}
	if ( !m_wndDialogBarRight.Create( this, IDD_DIALOGBAR_RIGHT, CBRS_RIGHT | WS_VISIBLE, IDD_DIALOGBAR_RIGHT ))
	{
		TRACE0("Failed to create dialogbar\n");
		return -1;      // fail to create
	}

	nPortNum = EnumCommPorts(aListStr);
	pPortList = (CComboBox *)m_wndDialogBarBottom.GetDlgItem(IDC_PORT_LIST);
	if(nPortNum > 0 )
	{
		for (int i=0; i<nPortNum; i++)
		{
			pPortList->AddString(aListStr[i]);
		}
	}
	pPortList->SetCurSel(0);

	pPortList = (CComboBox *)m_wndDialogBarBottom.GetDlgItem(IDC_SET_BITRATE);
	pPortList->SetCurSel(12);
	pPortList = (CComboBox *)m_wndDialogBarBottom.GetDlgItem(IDC_SET_STOPBIT);
	pPortList->SetCurSel(0);
	pPortList = (CComboBox *)m_wndDialogBarBottom.GetDlgItem(IDC_SET_FLOWCTL);
	pPortList->SetCurSel(0);
	pPortList = (CComboBox *)m_wndDialogBarBottom.GetDlgItem(IDC_SET_DATABIT);
	pPortList->SetCurSel(3);
	pPortList = (CComboBox *)m_wndDialogBarBottom.GetDlgItem(IDC_SET_VERIFYBIT);
	pPortList->SetCurSel(0);

//	(CCheckBox *)m_wndDialogBarBottom.GetDlgItem(IDC_SEND_RETURN)->;
	((CComboBox *)m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_SEND))->LimitText(1024);
	m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_TIMER)->SetWindowText("1000");
	InitCtl();
	SetDcb();

	// Set url font
	CFont	*pFont = new CFont;
	pFont->CreateFont(16, // nHeight 
					  10, // nWidth 
					  0, // nEscapement 
					  0, // nOrientation 
					  FW_BOLD, // nWeight 
					  TRUE, // bItalic 
					  TRUE, // bUnderline 
					  0, // cStrikeOut 
					  ANSI_CHARSET, // nCharSet 
					  OUT_DEFAULT_PRECIS, // nOutPrecision 
					  CLIP_DEFAULT_PRECIS, // nClipPrecision 
					  DEFAULT_QUALITY, // nQuality 
					  DEFAULT_PITCH | FF_SWISS, // nPitchAndFamily 
					  _T("Arial"));
	m_wndDialogBarBottom.GetDlgItem(IDC_STATIC_URL)->SetFont(pFont);

	delete	pFont;
//	SetTextColor(GetDC()->GetSafeHdc(), 1000);

//	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
//	EnableDocking(CBRS_ALIGN_ANY);
//	DockControlBar(&m_wndToolBar);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	WINDOWPLACEMENT	wndpl;
	WINDOWPLACEMENT	wndpl_ctl;
	CMDIFrameWnd::OnSize(nType, cx, cy);
	
	m_wndDialogBarBottom.GetWindowPlacement(&wndpl);	
	m_wndDialogBarBottom.GetDlgItem(IDC_GROUP_BOTTOM)->GetWindowPlacement(&wndpl_ctl);
	m_wndDialogBarBottom.GetDlgItem(IDC_GROUP_BOTTOM)->SetWindowPos(NULL, 
		wndpl_ctl.rcNormalPosition.left,
		wndpl_ctl.rcNormalPosition.top,
		wndpl.rcNormalPosition.right - 3,
		wndpl_ctl.rcNormalPosition.bottom - wndpl_ctl.rcNormalPosition.top,
		SWP_NOACTIVATE|SWP_NOOWNERZORDER|SWP_NOZORDER);
	m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_SEND)->GetWindowPlacement(&wndpl_ctl);
	m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_SEND)->SetWindowPos(NULL, 
		wndpl_ctl.rcNormalPosition.left,
		wndpl_ctl.rcNormalPosition.top,
		wndpl.rcNormalPosition.right - 132,
		wndpl_ctl.rcNormalPosition.bottom - wndpl_ctl.rcNormalPosition.top,
		SWP_NOACTIVATE|SWP_NOOWNERZORDER|SWP_NOZORDER);
	m_wndDialogBarBottom.GetDlgItem(IDC_GROUP_INFO)->GetWindowPlacement(&wndpl_ctl);
	m_wndDialogBarBottom.GetDlgItem(IDC_GROUP_INFO)->SetWindowPos(NULL, 
		wndpl_ctl.rcNormalPosition.left,
		wndpl_ctl.rcNormalPosition.top,
		wndpl.rcNormalPosition.right - 294,
		wndpl_ctl.rcNormalPosition.bottom - wndpl_ctl.rcNormalPosition.top,
		SWP_NOACTIVATE|SWP_NOOWNERZORDER|SWP_NOZORDER);
	m_wndDialogBarBottom.GetDlgItem(IDC_GROUP_MID)->GetWindowPlacement(&wndpl_ctl);
	m_wndDialogBarBottom.GetDlgItem(IDC_GROUP_MID)->SetWindowPos(NULL, 
		wndpl_ctl.rcNormalPosition.left,
		wndpl_ctl.rcNormalPosition.top,
		wndpl.rcNormalPosition.right - 3,
		wndpl_ctl.rcNormalPosition.bottom - wndpl_ctl.rcNormalPosition.top,
		SWP_NOACTIVATE|SWP_NOOWNERZORDER|SWP_NOZORDER);
}

void CMainFrame::OnOpenPort() 
{
	CBitmap bmpLoader;
	CChildFrame* pFrame = (CChildFrame*)GetActiveFrame();
	CDBGHelperView* pView = (CDBGHelperView*)pFrame->GetActiveView();

	if(pView->m_hDevice.IsOpened())
	{
		pView->m_hDevice.Close();
		m_wndDialogBarBottom.GetDlgItem(IDC_OPEN_PORT)->SetWindowText("打开串口(&O)");
		bmpLoader.LoadBitmap(IDB_PORT_CLOSE);
		((CStatic *)m_wndDialogBarBottom.GetDlgItem(IDC_PORT_STAT))->SetBitmap((HBITMAP)bmpLoader.Detach());
	}
	else
	{
		if(pView->Open())
		{
			m_wndDialogBarBottom.GetDlgItem(IDC_OPEN_PORT)->SetWindowText("关闭串口(&O)");
			bmpLoader.LoadBitmap(IDB_PORT_OPEN);
			((CStatic *)m_wndDialogBarBottom.GetDlgItem(IDC_PORT_STAT))->SetBitmap((HBITMAP)bmpLoader.Detach());
		}
		else
		{
//			m_wndDialogBarBottom.GetDlgItem(IDC_OPEN_PORT)->SetWindowText("打开串口(&O)");
//			bmpLoader.LoadBitmap(IDB_PORT_CLOSE);
//			((CStatic *)m_wndDialogBarBottom.GetDlgItem(IDC_PORT_STAT))->SetBitmap((HBITMAP)bmpLoader.Detach());
			MessageBox("Comm Port Open failed!");
		}
	}
}

int CMainFrame::EnumCommPorts(char (*pListStr)[MAX_COMM_NAME])
{
	HKEY hKey;
	LPCTSTR data_Set="HARDWARE\\DEVICEMAP\\SERIALCOMM\\";
	long ret0 = (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, data_Set, 0, KEY_READ, &hKey));
	
	if(ret0 != ERROR_SUCCESS)
	{
		return -1;
	}
	
	int i = 0;
	CHAR Name[50];
	LONG Status;
	
	UCHAR szPortName[MAX_COMM_NAME];
	DWORD dwIndex = 0;
	DWORD dwName;
	DWORD dwSizeofPortName;
	DWORD Type;
	
	do
	{
		dwName = sizeof(Name);
		dwSizeofPortName = sizeof(szPortName);
		Status = RegEnumValue(hKey, dwIndex++, Name, &dwName, NULL, &Type,
			szPortName, &dwSizeofPortName);
		if((Status == ERROR_SUCCESS)||(Status == ERROR_MORE_DATA))
		{ 
			if (pListStr != NULL)
			{
				szPortName[dwSizeofPortName] = 0;
				strcpy(*(pListStr + i), (LPCSTR)szPortName);
			}
			i++;
		}
	} while((Status == ERROR_SUCCESS)||(Status == ERROR_MORE_DATA));
	
	RegCloseKey(hKey);
	
	return i;
}

void CMainFrame::OnOpenFile() 
{
	CFileDialog	dlg( TRUE, "*.*", "", OFN_READONLY, "*.*||");
	if(dlg.DoModal() == IDOK)
	{
		m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_PATH)->SetWindowText(dlg.GetPathName());
	}
}

void CMainFrame::OnViewHelp() 
{
	
}

void CMainFrame::SetDcb()
{
	char	szTemp[16];

	m_wndDialogBarBottom.GetDlgItem(IDC_SET_BITRATE)->GetWindowText(szTemp, 16);
	m_dcb.BaudRate = atoi(szTemp);
	m_wndDialogBarBottom.GetDlgItem(IDC_SET_DATABIT)->GetWindowText(szTemp, 16);
	m_dcb.ByteSize = atoi(szTemp);
	m_wndDialogBarBottom.GetDlgItem(IDC_SET_STOPBIT)->GetWindowText(szTemp, 16);
	if (!strcmp(szTemp, "1"))
	{
		m_dcb.StopBits = 0;
	}
	else if (!strcmp(szTemp, "1.5"))
	{
		m_dcb.StopBits = 1;
	}
	else if (!strcmp(szTemp, "2"))
	{
		m_dcb.StopBits = 2;
	}
	else
	{
		TRACE("Invalid value of Stop bits !");
	}

	m_wndDialogBarBottom.GetDlgItem(IDC_SET_VERIFYBIT)->GetWindowText(szTemp, 16);
	if (!strcmp(szTemp, "None"))
	{
		m_dcb.Parity = NOPARITY;
	}
	else if (!strcmp(szTemp, "Even"))
	{
		m_dcb.Parity = EVENPARITY;
	}
	else if (!strcmp(szTemp, "Odd"))
	{
		m_dcb.Parity = ODDPARITY;
	}
	else if (!strcmp(szTemp, "Mark"))
	{
		m_dcb.Parity = MARKPARITY;
	}
	else if (!strcmp(szTemp, "Space"))
	{
		m_dcb.Parity = SPACEPARITY;
	}
	else
	{
		TRACE("Invalid value of Parity check !");
	}

	m_wndDialogBarBottom.GetDlgItem(IDC_SET_FLOWCTL)->GetWindowText(szTemp, 16);
	if (!strcmp(szTemp, "None"))
	{
		m_dcb.fOutxCtsFlow = 0;
		m_dcb.fOutxDsrFlow = 0;
		m_dcb.fRtsControl = 0;
		m_dcb.fTXContinueOnXoff = 0;
	}
	else if(strcmp(szTemp, "Hardware"))
	{
		m_dcb.fOutxCtsFlow = 0;
		m_dcb.fOutxDsrFlow = 0;
		m_dcb.fRtsControl = 0;
		m_dcb.fTXContinueOnXoff = 0;
	}
	else if (strcmp(szTemp, "Xon/Off"))
	{
		m_dcb.fOutxCtsFlow = 0;
		m_dcb.fOutxDsrFlow = 0;
		m_dcb.fRtsControl = 0;
		m_dcb.fTXContinueOnXoff = 0;
	}
	else
	{
		TRACE("Invalid value of flow control !");
	}
}

void CMainFrame::OnSelchangePortList() 
{
	CChildFrame* pFrame = (CChildFrame*)GetActiveFrame();
	CDBGHelperView* pView = (CDBGHelperView*)pFrame->GetActiveView();
	if(pView->m_hDevice.IsOpened())
	{
		pView->m_hDevice.Close();
		OnOpenPort();
	}
}

#define MAX_BYTE_NUM	1024
BOOL CMainFrame::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN && (pMsg->wParam == 'C' || pMsg->wParam == 'X' || pMsg->wParam == 'V' || pMsg->wParam == 'Z') && (GetActiveFrame()->GetActiveView() != GetFocus()))
	{
	   TranslateMessage(pMsg);
	   DispatchMessage(pMsg);
	   return TRUE;
	}

	if((pMsg->message == WM_KEYDOWN)
		&& (pMsg->wParam == VK_RETURN))
	{
		CComboBox*	pCombobox = ((CComboBox*)m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_SEND));
		CWnd*		pWnd = m_wndDialogBarBottom.GetFocus();
		
		if(pWnd)pWnd = pWnd->GetParent();
		
		if (pCombobox == pWnd)
		{
			char	szTemp[MAX_BYTE_NUM];
			CChildFrame* pFrame = (CChildFrame*)GetActiveFrame();
			CDBGHelperView* pView = (CDBGHelperView*)pFrame->GetActiveView();

			if(!pView->m_hDevice.IsOpened())
			{
				OnOpenPort();
				if(!pView->m_hDevice.IsOpened())
				{
					return CMDIFrameWnd::PreTranslateMessage(pMsg);
				}
			}

			m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_SEND)->GetWindowText(szTemp, MAX_BYTE_NUM);
			pView->m_hDevice.WriteComm(szTemp, strlen(szTemp));
			if (m_wndDialogBarBottom.IsDlgButtonChecked(IDC_SEND_RETURN))
			{
				pView->m_hDevice.WriteCommByte(VK_RETURN);
			}
			// delete same string, add string to the end
			if(pCombobox->SelectString(0, szTemp) != CB_ERR)
			{
				pCombobox->DeleteString(pCombobox->SelectString(0, szTemp));
			}
			pCombobox->InsertString(-1, szTemp);
			pCombobox->SetEditSel(0, -1);
		}
	}
	else if(pMsg->message == WM_LBUTTONUP)
	{
		CRect rcStatic; 
		CPoint ptCursor; 
		
		CWnd *pStatic=m_wndDialogBarBottom.GetDlgItem(IDC_STATIC_URL);
		pStatic->GetWindowRect(rcStatic);
		GetCursorPos(&ptCursor);
		
		if(rcStatic.PtInRect(ptCursor))
		{
			ShellExecute(m_hWnd,NULL,COMPANY_URL,NULL,NULL,SW_SHOWMAXIMIZED);
		}
	}
	else if((pMsg->message == WM_MOUSEWHEEL) && (GetActiveFrame()->GetActiveView() != GetFocus()))
	{
		GetActiveFrame()->GetActiveView()->SendMessage(pMsg->message, pMsg->wParam, pMsg->lParam);
	}
	return CMDIFrameWnd::PreTranslateMessage(pMsg);
}


void CMainFrame::OnExpandWin() 
{
	static bool IsShow=FALSE;

	m_wndDialogBarRight.ShowWindow(IsShow ? SW_SHOW:SW_HIDE);
	m_wndDialogBarBottom.GetDlgItem(IDC_EXPAND_WIN)->SetWindowText(IsShow ? "隐藏(&E)" : "扩展(&E)");
	IsShow = !IsShow;
	RecalcLayout();
}

void CMainFrame::OnInputSend()
{
	char	szTemp[MAX_BYTE_NUM];
	CChildFrame* pFrame = (CChildFrame*)GetActiveFrame();
	CDBGHelperView* pView = (CDBGHelperView*)pFrame->GetActiveView();
	
	if(!pView->m_hDevice.IsOpened() && !pView->Open())
	{
		return;
	}
	m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_SEND)->GetWindowText(szTemp, MAX_BYTE_NUM);
	pView->m_hDevice.WriteComm(szTemp, strlen(szTemp));
	if (m_wndDialogBarBottom.IsDlgButtonChecked(IDC_SEND_RETURN))
	{
		pView->m_hDevice.WriteCommByte(VK_RETURN);
	}
	
}

void CMainFrame::OnSendFile() 
{
	MessageBox("Not implemented, Please wait...");
}

void CMainFrame::OnButton1() 
{
	SendData(0);
}

void CMainFrame::OnButton2() 
{
	SendData(1);
}

void CMainFrame::OnButton3() 
{
	SendData(2);
}

void CMainFrame::OnButton4() 
{
	SendData(3);
}

void CMainFrame::OnButton5() 
{
	SendData(4);
}

void CMainFrame::OnButton6() 
{
	SendData(5);
}

void CMainFrame::OnButton7() 
{
	SendData(6);
}

void CMainFrame::OnButton8() 
{
	SendData(7);
}

void CMainFrame::OnButton9() 
{
	SendData(8);
}

void CMainFrame::OnButton10() 
{
	SendData(9);
}

void CMainFrame::OnButton11() 
{
	SendData(10);
}

void CMainFrame::OnButton12() 
{
	SendData(11);
}

void CMainFrame::OnButton13() 
{
	SendData(12);
}

void CMainFrame::OnButton14() 
{
	SendData(13);
}

void CMainFrame::OnButton15() 
{
	SendData(14);
}

void CMainFrame::OnButton16() 
{
	SendData(15);
}

void CMainFrame::OnButton17() 
{
	SendData(16);
}

void CMainFrame::OnButton18() 
{
	SendData(17);
}

void CMainFrame::OnButton19() 
{
	SendData(18);
}

void CMainFrame::OnButton20() 
{
	SendData(19);
}

void CMainFrame::OnButton21() 
{
	SendData(20);
}

void CMainFrame::OnButton22() 
{
	SendData(21);
}

void CMainFrame::OnButton23() 
{
	SendData(22);
}

void CMainFrame::OnButton24() 
{
	SendData(23);
}

void CMainFrame::OnButton25() 
{
	SendData(24);
}

void CMainFrame::OnButton26() 
{
	SendData(25);
}

void CMainFrame::OnButton27() 
{
	SendData(26);
}

void CMainFrame::OnButton28() 
{
	SendData(27);
}

void CMainFrame::OnButton29() 
{
	SendData(28);
}

void CMainFrame::OnButton30() 
{
	SendData(29);
}

void CMainFrame::OnButton31() 
{
	SendData(30);
}

void CMainFrame::OnButton32() 
{
	SendData(31);
}

void CMainFrame::InitCtl()
{
	CWinApp*	pApp = AfxGetApp();
	char		szItemName[16];
	CString		szTemp;

	nEditId[ 0] = IDC_EDIT1;
	nEditId[ 1] = IDC_EDIT2;
	nEditId[ 2] = IDC_EDIT3;
	nEditId[ 3] = IDC_EDIT4;
	nEditId[ 4] = IDC_EDIT5;
	nEditId[ 5] = IDC_EDIT6;
	nEditId[ 6] = IDC_EDIT7;
	nEditId[ 7] = IDC_EDIT8;
	nEditId[ 8] = IDC_EDIT9;
	nEditId[ 9] = IDC_EDIT10;
	nEditId[10] = IDC_EDIT11;
	nEditId[11] = IDC_EDIT12;
	nEditId[12] = IDC_EDIT13;
	nEditId[13] = IDC_EDIT14;
	nEditId[14] = IDC_EDIT15;
	nEditId[15] = IDC_EDIT16;
	nEditId[16] = IDC_EDIT17;
	nEditId[17] = IDC_EDIT18;
	nEditId[18] = IDC_EDIT19;
	nEditId[19] = IDC_EDIT20;
	nEditId[20] = IDC_EDIT21;
	nEditId[21] = IDC_EDIT22;
	nEditId[22] = IDC_EDIT23;
	nEditId[23] = IDC_EDIT24;
	nEditId[24] = IDC_EDIT25;
	nEditId[25] = IDC_EDIT26;
	nEditId[26] = IDC_EDIT27;
	nEditId[27] = IDC_EDIT28;
	nEditId[28] = IDC_EDIT29;
	nEditId[29] = IDC_EDIT30;
	nEditId[30] = IDC_EDIT31;
	nEditId[31] = IDC_EDIT32;

	nCheckId[ 0] = IDC_CHECK1;
	nCheckId[ 1] = IDC_CHECK2;
	nCheckId[ 2] = IDC_CHECK3;
	nCheckId[ 3] = IDC_CHECK4;
	nCheckId[ 4] = IDC_CHECK5;
	nCheckId[ 5] = IDC_CHECK6;
	nCheckId[ 6] = IDC_CHECK7;
	nCheckId[ 7] = IDC_CHECK8;
	nCheckId[ 8] = IDC_CHECK9;
	nCheckId[ 9] = IDC_CHECK10;
	nCheckId[10] = IDC_CHECK11;
	nCheckId[11] = IDC_CHECK12;
	nCheckId[12] = IDC_CHECK13;
	nCheckId[13] = IDC_CHECK14;
	nCheckId[14] = IDC_CHECK15;
	nCheckId[15] = IDC_CHECK16;
	nCheckId[16] = IDC_CHECK17;
	nCheckId[17] = IDC_CHECK18;
	nCheckId[18] = IDC_CHECK19;
	nCheckId[19] = IDC_CHECK20;
	nCheckId[20] = IDC_CHECK21;
	nCheckId[21] = IDC_CHECK22;
	nCheckId[22] = IDC_CHECK23;
	nCheckId[23] = IDC_CHECK24;
	nCheckId[24] = IDC_CHECK25;
	nCheckId[25] = IDC_CHECK26;
	nCheckId[26] = IDC_CHECK27;
	nCheckId[27] = IDC_CHECK28;
	nCheckId[28] = IDC_CHECK29;
	nCheckId[29] = IDC_CHECK30;
	nCheckId[30] = IDC_CHECK31;
	nCheckId[31] = IDC_CHECK32;
	
	for(int id=0; id<32; id++)
	{
		sprintf(szItemName, "ItemEdit%d", id);
		szTemp = pApp->GetProfileString("Settings", szItemName);
		if(szTemp.GetLength())
		{
			m_wndDialogBarRight.GetDlgItem(nEditId[id])->SetWindowText(szTemp);
		}
		sprintf(szItemName, "ItemCheck%d", id);
		int	nTmp = pApp->GetProfileInt("Settings", szItemName, 0);
		m_wndDialogBarRight.CheckDlgButton(nCheckId[id], nTmp ? TRUE : FALSE);
	}

	for(int i=0; i < sizeof(stCtlTable)/sizeof(CtlTable_t); i++)
	{
		szTemp = pApp->GetProfileString("Settings", stCtlTable[i].ItemName);
		if (szTemp.GetLength())
		{
			((CComboBox *)m_wndDialogBarBottom.GetDlgItem(stCtlTable[i].ItemId))->SelectString(-1, szTemp);
		}
	}
	if(pApp->GetProfileInt("Settings", "SendReturn", 1))
	{
		m_wndDialogBarBottom.CheckDlgButton(IDC_SEND_RETURN, TRUE);
	}
	szTemp = pApp->GetProfileString("Settings", "AutoTimer");
	if (szTemp.GetLength())
	{
		m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_TIMER)->SetWindowText(szTemp);
	}
}

void CMainFrame::SendData(int id)
{
	char	szTemp[MAX_BYTE_NUM];
	char	szItemName[16];
	CEdit*	pEdit;
	CWinApp* pApp = AfxGetApp();
	

	sprintf(szItemName, "ItemEdit%d", id);
	pEdit = (CEdit*)m_wndDialogBarRight.GetDlgItem(nEditId[id]);
	pEdit->GetWindowText(szTemp, MAX_BYTE_NUM);
	m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_SEND)->SetWindowText(szTemp);
	OnInputSend();
}

BOOL CMainFrame::DestroyWindow() 
{
	CWinApp* pApp = AfxGetApp();
	CEdit*	 pEdit;
	char	 szItemName[16];
	char	 szTemp[MAX_BYTE_NUM];
	int		 flag;

	for(int id=0; id<32; id++)
	{
		sprintf(szItemName, "ItemEdit%d", id);
		pEdit = (CEdit*)m_wndDialogBarRight.GetDlgItem(nEditId[id]);
		pEdit->GetWindowText(szTemp, MAX_BYTE_NUM);
		pApp->WriteProfileString("Settings", szItemName, szTemp);

		int	nTmp = m_wndDialogBarRight.IsDlgButtonChecked(nCheckId[id]);
		sprintf(szItemName, "ItemCheck%d", id);
		pApp->WriteProfileInt("Settings", szItemName, nTmp);
	}

	for(int i=0; i < sizeof(stCtlTable)/sizeof(CtlTable_t); i++)
	{
		m_wndDialogBarBottom.GetDlgItem(stCtlTable[i].ItemId)->GetWindowText(szTemp, MAX_BYTE_NUM);
		pApp->WriteProfileString("Settings", stCtlTable[i].ItemName, szTemp);
	}
	flag = m_wndDialogBarBottom.IsDlgButtonChecked(IDC_SEND_RETURN);
	pApp->WriteProfileInt("Settings", "SendReturn", flag);

	m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_TIMER)->GetWindowText(szTemp, MAX_BYTE_NUM);
	pApp->WriteProfileString("Settings", "AutoTimer", szTemp);

	return CMDIFrameWnd::DestroyWindow();
}

void CMainFrame::OnInputSetfocus() 
{
	CChildFrame* pFrame = (CChildFrame*)GetActiveFrame();
	CDBGHelperView* pView = (CDBGHelperView*)pFrame->GetActiveView();

	if(GetFocus() == pView)
	{
		m_wndDialogBarBottom.GetDlgItem(IDC_EDIT_SEND)->SetFocus();
	}
	else
	{
		pView->SetFocus();
	}
}

void CMainFrame::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CRect rcStatic; 
	CPoint ptCursor; 
	
	CWnd *pStatic=GetDlgItem(IDC_STATIC_URL);
	pStatic->GetWindowRect(rcStatic);
	GetCursorPos(&ptCursor);
	
	if(rcStatic.PtInRect(ptCursor))
	{
		ShellExecute(m_hWnd,NULL,"mailto:cj@cool.com.cn",NULL,NULL,SW_SHOWMAXIMIZED);
	}

	
	CMDIFrameWnd::OnLButtonUp(nFlags, point);
}

void CMainFrame::OnEditCut() 
{
	MessageBox("aaa");
}

void CMainFrame::OnItemClear() 
{
	CEdit*	pEdit;
	char	szItemName[20];

	for (int id=0; id<32; id++)
	{
		sprintf(szItemName, "ItemEdit%d", id);
		pEdit = (CEdit*)m_wndDialogBarRight.GetDlgItem(nEditId[id]);
		pEdit->SetWindowText("");
	}
	
}


void CMainFrame::OnSelchangeSetBitrate() 
{
	CChildFrame* pFrame = (CChildFrame*)GetActiveFrame();
	CDBGHelperView* pView = (CDBGHelperView*)pFrame->GetActiveView();

	pView->Open();
}

void CMainFrame::OnEnableTimer() 
{
	int		iTimer;
	if(m_wndDialogBarBottom.IsDlgButtonChecked(IDC_ENABLE_TIMER))
	{
		iTimer = m_wndDialogBarBottom.GetDlgItemInt(IDC_EDIT_TIMER);
		SetTimer(TIMER_ID_AUTO_SEND, iTimer, NULL);
	}
	else
	{
		KillTimer(TIMER_ID_AUTO_SEND);
	}

}

void CMainFrame::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == TIMER_ID_AUTO_SEND)
	{
		OnInputSend();
	}
	CMDIFrameWnd::OnTimer(nIDEvent);
}

LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if(message == WM_DEVICECHANGE)
	{
		if((wParam == DBT_DEVICEARRIVAL) || (wParam == DBT_DEVICEREMOVECOMPLETE))
		{
			int		nPortNum = 0;
			char	aListStr[MAX_COMM_NUM][MAX_COMM_NAME];
			char	szTemp[MAX_COMM_NAME];
			CComboBox*	pPortList;

			nPortNum = EnumCommPorts(aListStr);
			pPortList = (CComboBox *)m_wndDialogBarBottom.GetDlgItem(IDC_PORT_LIST);
			nPortNum = pPortList->GetCount()>nPortNum ? pPortList->GetCount() : nPortNum;
			if(nPortNum > 0 )
			{
				for (int i=0; i<nPortNum; i++)
				{
					pPortList->GetLBText(i,szTemp);
					if(strcmp(szTemp, aListStr[i]))
					{
						if(wParam == DBT_DEVICEARRIVAL)
						{
							TRACE("device inserted\n");
							pPortList->InsertString(i, aListStr[i]);
						}
						else
						{
							TRACE("device removed\n");
							if(pPortList->GetCurSel() == i)
							{
								OnOpenPort();
							}
							pPortList->DeleteString(i);
						}
					}
				}
			}
//			pPortList->SetCurSel(0);
		}
	}
	
	return CMDIFrameWnd::WindowProc(message, wParam, lParam);
}
