// Serial.cpp

#include "stdafx.h"
#include "Serial.h"
#include "MainFrm.h"
#include "resource.h"
void ErrorTrace(DWORD	ErrorCode);

CSerial::CSerial()
{
	
	memset( &m_OverlappedRead, 0, sizeof( OVERLAPPED ) );
	memset( &m_OverlappedWrite, 0, sizeof( OVERLAPPED ) );
	m_hIDComDev = NULL;
	m_bOpened = FALSE;
	
}

CSerial::~CSerial()
{
	Close();
}

BOOL CSerial::Open( char szPort[16], int nBaud )
{
	char	szPortTemp[16];
	strcpy(szPortPrev, szPort);
	nBaudPrev = nBaud;

	if( m_bOpened )
	{
		GetCommState( m_hIDComDev, &dcb );
		dcb.BaudRate = nBaud;
		dcb.fDtrControl = DTR_CONTROL_DISABLE;		// 关闭流控
		dcb.fOutxDsrFlow = FALSE;
		dcb.fOutxCtsFlow = FALSE;

		if(!SetCommState( m_hIDComDev, &dcb ))return ( FALSE );
		SetCommMask( m_hIDComDev, EV_RXCHAR );
		return( TRUE );
	}
	
	
	wsprintf( szPortTemp, "\\\\.\\%s", szPort );
	m_hIDComDev = CreateFile( szPortTemp, 
		GENERIC_READ | GENERIC_WRITE, 
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
		NULL );
	if( m_hIDComDev == NULL ) return( FALSE );
	memset( &m_OverlappedRead, 0, sizeof( OVERLAPPED ) );
	memset( &m_OverlappedWrite, 0, sizeof( OVERLAPPED ) );
	
	COMMTIMEOUTS CommTimeOuts;
	CommTimeOuts.ReadIntervalTimeout = 0xFFFFFFFF;
	CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
	CommTimeOuts.ReadTotalTimeoutConstant = 1000;
	CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
	CommTimeOuts.WriteTotalTimeoutConstant = 5000;
	SetCommTimeouts( m_hIDComDev, &CommTimeOuts );
	
//	wsprintf( szComParams, "COM%d:%d,n,8,1", nPort, nBaud );
	
	m_OverlappedRead.hEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
	m_OverlappedWrite.hEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
	
	dcb.DCBlength = sizeof( DCB );
	GetCommState( m_hIDComDev, &dcb );
	dcb.BaudRate = nBaud;
	dcb.ByteSize = 8;
	if( !SetCommState( m_hIDComDev, &dcb ) ||
		!SetupComm( m_hIDComDev, 0x10000, 0x10000 ) ||
		m_OverlappedRead.hEvent == NULL ||
		m_OverlappedWrite.hEvent == NULL ){
		DWORD dwError = GetLastError();
		ErrorTrace(dwError);
		if( m_OverlappedRead.hEvent != NULL ) CloseHandle( m_OverlappedRead.hEvent );
		if( m_OverlappedWrite.hEvent != NULL ) CloseHandle( m_OverlappedWrite.hEvent );
		CloseHandle( m_hIDComDev );
		return( FALSE );
	}
	
	PurgeComm( m_hIDComDev, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR );
	m_bOpened = TRUE;
	
	if(!SetCommMask( m_hIDComDev, EV_RXCHAR ))
	{
		AfxMessageBox("SetCommMask failed\n");
	}
	return( m_bOpened );
	
}

BOOL CSerial::Close( void )
{
	if( !m_bOpened || m_hIDComDev == NULL ) return( TRUE );
	
	if( m_OverlappedRead.hEvent != NULL ) CloseHandle( m_OverlappedRead.hEvent );
	if( m_OverlappedWrite.hEvent != NULL ) CloseHandle( m_OverlappedWrite.hEvent );
	CloseHandle( m_hIDComDev );
	m_bOpened = FALSE;
	m_hIDComDev = NULL;
	
	return( TRUE );
}

BOOL CSerial::WriteCommByte( unsigned char ucByte )
{
	BOOL bWriteStat;
	DWORD dwBytesWritten;
	
	bWriteStat = WriteFile( m_hIDComDev, (LPSTR) &ucByte, 1, &dwBytesWritten, &m_OverlappedWrite );
	if( !bWriteStat && ( GetLastError() == ERROR_IO_PENDING ) ){
		if( WaitForSingleObject( m_OverlappedWrite.hEvent, 1000 ) ) dwBytesWritten = 0;
		else{
			GetOverlappedResult( m_hIDComDev, &m_OverlappedWrite, &dwBytesWritten, FALSE );
			m_OverlappedWrite.Offset += dwBytesWritten;
		}
	}
	
	return( TRUE );
}

int	CSerial::WriteComm( const char *psBuf, int nSize )
{
	int				nRetCode;
	DWORD			dwErrorCode, nWrite;
	COMSTAT			ComStat;
	DWORD			dwErrorFlags;
	CString			sTemp;
	
	nRetCode = WriteFile( m_hIDComDev, psBuf, nSize, &nWrite, &m_OverlappedWrite );
	if ( !nRetCode )
	{
		dwErrorCode = GetLastError();
		if ( dwErrorCode == ERROR_IO_PENDING )
		{
			//			printf( "IO pending...\n" );
			while ( !GetOverlappedResult( m_hIDComDev, &m_OverlappedWrite, &nWrite, TRUE ))
			{
				dwErrorCode	= GetLastError();
				if ( dwErrorCode == ERROR_IO_INCOMPLETE )
					continue;
				else
				{
					nWrite	= -1;
					ClearCommError( m_hIDComDev, &dwErrorFlags, &ComStat );
					sTemp.Format( "Write error: %d\n", dwErrorCode );
					//					MessageBox( sTemp);//, "提示" );
					break;
				}
			}
		}
		else
		{
			nWrite = -2;
			ClearCommError( m_hIDComDev, &dwErrorFlags, &ComStat );
			sTemp.Format( "Write error: %d\n", dwErrorCode );
			//			MessageBox( sTemp);//, "提示" );
		}
	}
	
	return nWrite;
}
int CSerial::SendData( const char *buffer, int size )
{
	
	if( !m_bOpened || m_hIDComDev == NULL ) return( 0 );
	
	DWORD dwBytesWritten;
	BOOL bWriteStat = WriteFile( m_hIDComDev, (LPSTR) buffer, size, &dwBytesWritten, &m_OverlappedWrite );
	/*
	DWORD dwBytesWritten = 0;
	int i;
	for( i=0; i<size; i++ ){
	WriteCommByte( buffer[i] );
	dwBytesWritten++;
	}
	
	*/	return( (int) dwBytesWritten );
	
}

int CSerial::ReadDataWaiting( void )
{
	
	if( !m_bOpened || m_hIDComDev == NULL ) return( 0 );
	
	DWORD dwErrorFlags;
	COMSTAT ComStat;
	
	ClearCommError( m_hIDComDev, &dwErrorFlags, &ComStat );
	
	return( (int) ComStat.cbInQue );
	
}

int CSerial::ReadData( void *buffer, int limit )
{
	
	if( !m_bOpened || m_hIDComDev == NULL ) return( 0 );
	
	BOOL bReadStatus;
	DWORD dwBytesRead, dwErrorFlags;
	COMSTAT ComStat;
	
	if(!ClearCommError( m_hIDComDev, &dwErrorFlags, &ComStat ))
	{
		DWORD er = GetLastError();
		int a;
		a = 0;
	}
	if( !ComStat.cbInQue ) return( 0 );
	
	dwBytesRead = (DWORD) ComStat.cbInQue;
	if( limit < (int) dwBytesRead ) dwBytesRead = (DWORD) limit;
	
	dwBytesRead = 0;
	bReadStatus = ReadFile( m_hIDComDev, buffer, limit, &dwBytesRead, &m_OverlappedRead );
	if( !bReadStatus )
	{
		if( GetLastError() == ERROR_IO_PENDING )
		{
			WaitForSingleObject( m_OverlappedRead.hEvent, 2000 );
			return( (int) dwBytesRead );
		}
		return( 0 );
	}
	
	return( (int) dwBytesRead );
	
}
