// Serial.h

#ifndef __SERIAL_H__
#define __SERIAL_H__

#define FC_DTRDSR       0x01
#define FC_RTSCTS       0x02
#define FC_XONXOFF      0x04
#define ASCII_BEL       0x07
#define ASCII_BS        0x08
#define ASCII_LF        0x0A
#define ASCII_CR        0x0D
#define ASCII_XON       0x11
#define ASCII_XOFF      0x13

class CSerial
{

public:
	UINT m_nTransmitedBytes;
	UINT m_nReceivedBytes;
	int WriteComm(const char *psBuf, int nSize);
	CSerial();
	~CSerial();

	BOOL Open( char szPort[16], int nBaud = 9600 );
	BOOL Close( void );

	int ReadData( void *, int );
	int SendData( const char *, int );
	int ReadDataWaiting( void );
	DCB dcb;
	int nBaudPrev;
	char szPortPrev[16];
	HANDLE m_hIDComDev;

	BOOL IsOpened( void ){ return( m_bOpened ); }
	BOOL WriteCommByte( unsigned char );

protected:

	OVERLAPPED m_OverlappedRead, m_OverlappedWrite;
	BOOL m_bOpened;

};

#endif
