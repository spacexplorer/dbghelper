// DBGHelper.h : main header file for the DBGHELPER application
//

#if !defined(AFX_DBGHELPER_H__A8F5FE11_2A69_4C2E_8CEF_498129BFABAD__INCLUDED_)
#define AFX_DBGHELPER_H__A8F5FE11_2A69_4C2E_8CEF_498129BFABAD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperApp:
// See DBGHelper.cpp for the implementation of this class
//

class CDBGHelperApp : public CWinApp
{
public:
	CDBGHelperApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDBGHelperApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CDBGHelperApp)
	afx_msg void OnAppAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBGHELPER_H__A8F5FE11_2A69_4C2E_8CEF_498129BFABAD__INCLUDED_)
