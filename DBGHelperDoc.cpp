// DBGHelperDoc.cpp : implementation of the CDBGHelperDoc class
//

#include "stdafx.h"
#include "DBGHelper.h"

#include "DBGHelperDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperDoc

IMPLEMENT_DYNCREATE(CDBGHelperDoc, CDocument)

BEGIN_MESSAGE_MAP(CDBGHelperDoc, CDocument)
	//{{AFX_MSG_MAP(CDBGHelperDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperDoc construction/destruction

CDBGHelperDoc::CDBGHelperDoc()
{
}

CDBGHelperDoc::~CDBGHelperDoc()
{
}

BOOL CDBGHelperDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CDBGHelperDoc serialization

void CDBGHelperDoc::Serialize(CArchive& ar)
{
	((CEditView*)m_viewList.GetHead())->SerializeRaw(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperDoc diagnostics

#ifdef _DEBUG
void CDBGHelperDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDBGHelperDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDBGHelperDoc commands
