// DBGHelperDoc.h : interface of the CDBGHelperDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DBGHELPERDOC_H__D5FC37CB_19DA_453C_A265_39A795655F82__INCLUDED_)
#define AFX_DBGHELPERDOC_H__D5FC37CB_19DA_453C_A265_39A795655F82__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CDBGHelperDoc : public CDocument
{
protected: // create from serialization only
	CDBGHelperDoc();
	DECLARE_DYNCREATE(CDBGHelperDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDBGHelperDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDBGHelperDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CDBGHelperDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBGHELPERDOC_H__D5FC37CB_19DA_453C_A265_39A795655F82__INCLUDED_)
